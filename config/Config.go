package config

import (
	"github.com/spf13/viper"
)

func Load() {

	// MongoDb
	viper.SetDefault("MONGO_CON_STRING", "mongodb://localhost:27017/")
	_ = viper.BindEnv("MONGO_CON_STRING")

	// Min.io
	viper.SetDefault("MINIO_SECRET_NAME", "minio")
	_ = viper.BindEnv("MINIO_SECRET_NAME")

	// External APIs
	viper.SetDefault("CATALOG_API", "http://catalog:9091")
	_ = viper.BindEnv("CATALOG_API")
}
