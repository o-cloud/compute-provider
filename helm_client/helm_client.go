package helm_client

import (
	"compute/kubernetes_client"
	"fmt"
	"log"
	"os"

	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/release"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

func loadChart(chartName string) (*chart.Chart, error) {
	chartPath := "/data/helm/" + chartName + "/"
	chart, err := loader.Load(chartPath)
	if err != nil {
		fmt.Println(err)
	}
	return chart, err
}

func getInstallClient(namespace string, releaseName string) *action.Install {
	// Retrieve kubernetes config for Helm
	actionConfig, err := getActionConfig(namespace)
	if err != nil {
		fmt.Println(err)
	}
	installClient := action.NewInstall(actionConfig)
	installClient.Namespace = namespace
	installClient.ReleaseName = releaseName
	return installClient
}

func getActionConfig(namespace string) (*action.Configuration, error) {
	log.Println("Get action config")
	actionConfig := new(action.Configuration)
	var kubeConfig *genericclioptions.ConfigFlags

	// Create the rest config instance with ServiceAccount values loaded in them
	ClusterConfig, err := kubernetes_client.GetConfig()
	if err != nil {
		return nil, err
	}

	// Create the ConfigFlags struct instance with initialized values from ServiceAccount
	kubeConfig = genericclioptions.NewConfigFlags(false)
	kubeConfig.APIServer = &ClusterConfig.Host
	kubeConfig.BearerToken = &ClusterConfig.BearerToken
	kubeConfig.CAFile = &ClusterConfig.CAFile
	kubeConfig.Namespace = &namespace
	if err := actionConfig.Init(kubeConfig, namespace, os.Getenv("HELM_DRIVER"), log.Printf); err != nil {
		return nil, err
	}
	log.Println("autoconfig ready!")
	return actionConfig, nil
}

func InstallTargetChartOnSourceCluster(targetName string, secretName string, releaseNamespace string) {
	log.Println("Installing target chart on source cluster")
	chart, err := loadChart("admiralty_source")

	// Set release parameters
	releaseName := "sourcehelminstall" + targetName
	releaseName = kubernetes_client.SanitizeString(releaseName)

	installClient := getInstallClient(releaseNamespace, releaseName)

	// Define values for installation
	vals := map[string]interface{}{
		"admiralty_ressource_type": "Target",
		"secret_name":              kubernetes_client.SanitizeString(secretName),
	}

	var rel *release.Release
	rel, err = installClient.Run(chart, vals)

	if err != nil {
		fmt.Errorf(err.Error())
		installClient.IsUpgrade = true
		rel, err = installClient.Run(chart, vals)
	}

	if err != nil {
		fmt.Println(err)
	}

	if rel != nil {
		log.Printf("Successfully installed release: %s\n", rel.Name)
	}
}

func InstallSourceChartOnTargetCluster(serviceAccountName string, releaseNamespace string) {
	log.Println("Installing source chart on target cluster, in namespace", releaseNamespace)
	// Load charts
	chart, err := loadChart("admiralty_target")

	// Set release parameter
	releaseName := "targethelminstall-" + serviceAccountName
	releaseName = kubernetes_client.SanitizeString(releaseName)

	installClient := getInstallClient(releaseNamespace, releaseName)

	sourceType := "Source"
	rbac := 0
	if releaseNamespace != "default" {
		log.Println("Non default cluster!!!!")
		sourceType = "ClusterSource"
		rbac = 1
	}

	vals := map[string]interface{}{
		"admiralty_ressource_type": sourceType,
		"serviceaccount_target":    serviceAccountName,
		"namespace_target":         releaseNamespace,
		"admiralty_source_name":    releaseName,
		"rbac":                     rbac,
	}

	var rel *release.Release
	rel, err = installClient.Run(chart, vals)
	if err != nil {
		fmt.Errorf(err.Error())
		installClient.IsUpgrade = true
		rel, err = installClient.Run(chart, vals)
	}

	if err != nil {
		fmt.Println(err)
	}

	if rel != nil {
		log.Println(rel)
		log.Printf("Successfully installed release: %s\n", rel.Name)
	}
}
