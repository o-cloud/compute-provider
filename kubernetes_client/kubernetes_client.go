/*
Kubernetes client to interact with serviceaccounts.
The point so far is to :
	- create the SA
	- retrieve the token for Admiralty
	- Retrieve the URL of the kubernetes API for Admiralty
*/
package kubernetes_client

import (
	"compute/compute_model"
	"encoding/base64"
	"io/ioutil"
	"math/rand"
	"strconv"
	"strings"

	"github.com/spf13/viper"

	v1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"

	//	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"time"

	//	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	// This is required to connect to GKE cluster
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	//"k8s.io/client-go/util/retry"
	//
	// Uncomment to load all auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth"
	//
	// Or uncomment to load specific auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth/azure"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/openstack"
)

func GetConfig() (*rest.Config, error) {
	log.Println("Trying to get config from wihtin the cluster")
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Println("Error retrieving kubernetes config:", err)
	}

	// It seems that from inside the cluster we need to get the ca.crt file and
	// retrieve the cert from there.
	certs, err := ioutil.ReadFile(config.CAFile)
	if err != nil {
		log.Fatalf("Failed to append %q to RootCAs: %v", config.CAFile, err)
	}
	config.CAData = certs
	return config, err
}

func GetNamespaceFromUrl(inputUrl string) string {
	urlParse, err := url.Parse(inputUrl)
	if err != nil {
		log.Fatal(err)
	}
	queryParams := urlParse.Query()
	namespace := queryParams["namespace"][0]
	return namespace
}
func GenerateServiceAccountName(name string) string {
	log.Printf("Generete service account name")
	serviceAccountName := name + "-sa-" + strconv.Itoa(rand.Intn(1000000))
	return serviceAccountName
}

func GetServiceAccountSecret(serviceAccountName string, namespace string, clientset *kubernetes.Clientset) (*apiv1.Secret, error) {
	log.Printf("Retrieving secret from service account %s\n", serviceAccountName)
	serviceAccountInfo, err := clientset.CoreV1().ServiceAccounts(namespace).Get(context.Background(), serviceAccountName, metav1.GetOptions{})
	secretName := serviceAccountInfo.Secrets[0].Name
	ServiceAccountSecret, err := clientset.CoreV1().Secrets(namespace).Get(context.Background(), secretName, metav1.GetOptions{})
	return ServiceAccountSecret, err
}

func GetServiceAccountTokenFromSecret(serviceAccountSecret *apiv1.Secret) string {
	log.Println("Retrieve service account token from secret")
	serviceAccountToken := string(serviceAccountSecret.Data["token"])
	return serviceAccountToken
}

func testErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
func DeployServiceAccount(serviceAccountName string, namespace string, config *rest.Config) (compute_model.ResponseToken, error) {
	// Method to create an SA and retrieve info required by Admiralty
	log.Printf("Deploying the service account %s\n", serviceAccountName)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	var response compute_model.ResponseToken
	response.ServiceAccountName = serviceAccountName
	serviceAccount, err := CreateServiceAccount(serviceAccountName, namespace, clientset)
	testErr(err)
	serviceAccountSecret, err := GetServiceAccountSecret(serviceAccount.Name, namespace, clientset)
	testErr(err)
	err = CreateRbacForClientSa(serviceAccountName, namespace, clientset)
	testErr(err)
	tokenString := GetServiceAccountTokenFromSecret(serviceAccountSecret)
	response.Token = tokenString
	response.CertificateData = base64.StdEncoding.EncodeToString(config.CAData)
	return response, nil

}

func CreateRbacForClientSa(saName string, namespace string, clientset *kubernetes.Clientset) error {
	roleName := saName + "-role"
	_, err := clientset.RbacV1().Roles(namespace).Create(context.Background(), &v1.Role{
		ObjectMeta: metav1.ObjectMeta{
			Name: roleName,
		},
		Rules: []v1.PolicyRule{
			{
				Verbs:     []string{"deletecollection"},
				APIGroups: []string{"multicluster.admiralty.io"},
				Resources: []string{"podchaperons"},
			},
			{
				Verbs:     []string{"list", "get", "watch"},
				APIGroups: []string{""},
				Resources: []string{"pods", "pods/log"},
			},
		},
	}, metav1.CreateOptions{})

	if err != nil {
		return err
	}

	_, err = clientset.RbacV1().RoleBindings(namespace).Create(context.Background(), &v1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{Name: saName + "-role-binding"},
		Subjects: []v1.Subject{
			{
				Kind: "ServiceAccount",
				Name: saName,
			},
		},
		RoleRef: v1.RoleRef{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "Role",
			Name:     roleName,
		},
	}, metav1.CreateOptions{})
	return err
}

func CreateRbacForArgo(namespace string, config *rest.Config) error {
	name := "argo-workflow"
	role := &v1.Role{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Rules: []v1.PolicyRule{
			{
				Verbs:     []string{"get", "watch", "patch"},
				APIGroups: []string{""},
				Resources: []string{"pods"},
			},
			{
				Verbs:     []string{"get", "watch"},
				APIGroups: []string{""},
				Resources: []string{"pods/log"},
			},
			{
				Verbs:     []string{"get"},
				APIGroups: []string{""},
				Resources: []string{"secrets"},
			},
		},
	}
	roleBinding := &v1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Subjects: []v1.Subject{
			{
				Kind: "ServiceAccount",
				Name: name,
			},
		},
		RoleRef: v1.RoleRef{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "Role",
			Name:     name,
		},
	}
	sa := &apiv1.ServiceAccount{ObjectMeta: metav1.ObjectMeta{Name: name}}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Println(err)
	}

	_, err = clientset.CoreV1().ServiceAccounts(namespace).Create(context.Background(), sa, metav1.CreateOptions{})
	if err != nil {
		return fmt.Errorf("cannot create service account for argo in namespace %s, %s", namespace, err)
	}
	_, err = clientset.RbacV1().Roles(namespace).Create(context.Background(), role, metav1.CreateOptions{})
	if err != nil {
		return fmt.Errorf("cannot create role for argo in namespace %s, %s", namespace, err)
	}
	_, err = clientset.RbacV1().RoleBindings(namespace).Create(context.Background(), roleBinding, metav1.CreateOptions{})
	if err != nil {
		return fmt.Errorf("cannot create rolebinding for argo in namespace %s, %s", namespace, err)
	}
	return nil
}

func CreateNameSpace(namespace string, config *rest.Config) error {
	// Create NameSpace Object
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Println(err)
	}
	log.Printf("Create new namespace %s\n", namespace)
	newNameSpace := &apiv1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: namespace}}

	// Create the serviceaccount
	_, err = clientset.CoreV1().Namespaces().Create(context.Background(), newNameSpace, metav1.CreateOptions{})
	if err != nil {
		fmt.Println(err)
		//panic(err)
	}
	return err
}

func CreateServiceAccount(serviceAccountName string, namespace string, clientset *kubernetes.Clientset) (*apiv1.ServiceAccount, error) {
	// Create ServiceAccount Object
	log.Printf("Create new service account %s\n", serviceAccountName)
	newServiceAccount := &apiv1.ServiceAccount{ObjectMeta: metav1.ObjectMeta{Name: serviceAccountName}}

	// Create the serviceaccount
	serviceAccount, err := clientset.CoreV1().ServiceAccounts(namespace).Create(context.Background(), newServiceAccount, metav1.CreateOptions{})
	if err != nil {
		fmt.Println(err)
	}

	// Wait until service account is fully ready
	for len(serviceAccount.Secrets) == 0 {
		serviceAccount, err = clientset.CoreV1().ServiceAccounts(namespace).Get(context.Background(), serviceAccountName, metav1.GetOptions{})
		testErr(err)
		time.Sleep(10 * time.Millisecond)
	}
	return serviceAccount, err
}

func ApplySecret(newSecret *apiv1.Secret, namespace string, config *rest.Config) {
	// Method to apply the secret to the cluster from
	// a Secret object
	log.Printf("Applying secret %s\n", newSecret.Name)
	fmt.Println(newSecret)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Println(err)
	}

	// Apply the secret to the k8s cluster
	clientset.CoreV1().Secrets(namespace).Create(context.Background(), newSecret, metav1.CreateOptions{})
}

func BuildSecretFromServiceAccountInfo(responseToken compute_model.ResponseToken) (*apiv1.Secret, error) {
	// Method to build a secret object needed by the Source cluster.
	// The secret is built from the APIUrl, token, ServiceAccount name and certificate
	// of the target cluster
	log.Println("Building secret from service account info")
	ApiURL := responseToken.ApiUrl
	Token := responseToken.Token
	ServiceAccount := responseToken.ServiceAccountName
	CAData64 := responseToken.CertificateData
	TargetName := responseToken.TargetName
	namespace := responseToken.Namespace

	// Label for Admiralty application in secret
	label := map[string]string{
		"application": "admiralty",
	}

	secretData := string(PrepareConfigForSecret(ApiURL, Token, ServiceAccount, CAData64, TargetName))
	newSecret := &apiv1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			//Name: "secret-gke-irt-sb-us-central1-c-cluster-sbo2"  ,
			Name:      "secret-" + TargetName,
			Namespace: namespace,
			Labels:    label,
		},
		Type:       "Opaque",
		StringData: map[string]string{"config": secretData},
	}
	return newSecret, nil
}

func PrepareConfigForSecret(ApiURL string, Token string, ServiceAccount string, CAData64 string, TargetName string) []byte {
	// Methode to prepare the Config that is to be set in the secret

	log.Println("Preparing secret")

	var Preferences struct{}
	ClusterStruct := Cluster{ApiURL, CAData64}
	ClustersStruct := []Clusters{{Name: TargetName}, {Cluster: ClusterStruct}}
	UserStruct := User{Token}
	UsersStruct := []Users{{Name: TargetName}, {User: UserStruct}}
	ContextStruct := Context{TargetName, TargetName}
	ContextsStruct := []Contexts{{Name: TargetName}, {Context: ContextStruct}}
	DataSecretStruct := DataSecret{"Config", "v1", Preferences, ClustersStruct, UsersStruct, ContextsStruct, TargetName}

	config, err := json.MarshalIndent(DataSecretStruct, "", "  ")

	if err != nil {
		fmt.Println(err)
	}

	return config
}

func AdmiraltyLabelNamespace(ns string, config *rest.Config) {

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Println(err)
	}

	namespace, err := clientset.CoreV1().Namespaces().Get(context.Background(), ns, metav1.GetOptions{})
	label := map[string]string{
		"multicluster-scheduler": "enabled",
	}

	log.Printf("Namespace %s labeled for Admiralty execution\n", namespace.Name)
	namespace.Labels = label
	clientset.CoreV1().Namespaces().Update(context.Background(), namespace, metav1.UpdateOptions{})
}

func SetRuntimeContainer(config *rest.Config) {

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	// Retrieve config map to update
	configmap, err := clientset.CoreV1().ConfigMaps("argo").Get(context.Background(), "workflow-controller-configmap", metav1.GetOptions{})
	if err != nil {
		panic(err)
	}

	// Create new value for configmap
	dataContainer := map[string]string{
		"containerRuntimeExecutor": "k8sapi",
	}

	// Set value to configmap
	configmap.Data = dataContainer

	log.Println("Execution Runtime Container Set!")

	// Update configmap
	clientset.CoreV1().ConfigMaps("argo").Update(context.Background(), configmap, metav1.UpdateOptions{})
}

func DeleteServiceAccount(serviceaccountName string, namespace string, config *rest.Config) {
	log.Printf("Deleting service account %s\n", serviceaccountName)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Println(err)
	}
	err = clientset.CoreV1().ServiceAccounts(namespace).Delete(context.Background(), serviceaccountName, metav1.DeleteOptions{})
	if err != nil {
		fmt.Println(err)
	}
}

func DeleteNamespace(namespace string, config *rest.Config) {
	log.Printf("Deleting namespace %s\n", namespace)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Println(err)
	}
	err = clientset.CoreV1().Namespaces().Delete(context.Background(), namespace, metav1.DeleteOptions{})
	if err != nil {
		fmt.Println(err)
	}
}

func GetKubernetesIP() (string, int32) {
	c, err := GetConfig()
	if err != nil {
		log.Fatalln(err)
	}
	client, err := kubernetes.NewForConfig(c)
	if err != nil {
		log.Fatalln(err)
	}
	list, err := client.CoreV1().Endpoints("default").Get(context.TODO(), "kubernetes", metav1.GetOptions{})
	if err != nil {
		log.Fatalln(err)
	}
	if len(list.Subsets) == 0 || len(list.Subsets[0].Addresses) == 0 {
		log.Fatalln("cannot find kubernetes endpoint")
	}

	return list.Subsets[0].Addresses[0].IP, list.Subsets[0].Ports[0].Port
}

func GetKubernetesEndpoint() string {
	ip, port := GetKubernetesIP()
	return "https://" + ip + ":" + fmt.Sprint(port)
}

func GetS3Keys() (string, string) {
	c, err := GetConfig()
	if err != nil {
		log.Fatalln(err)
	}
	client, err := kubernetes.NewForConfig(c)
	if err != nil {
		log.Fatalln(err)
	}
	minioSecretName := viper.GetString("MINIO_SECRET_NAME")
	secret, err := client.CoreV1().Secrets("default").Get(context.Background(), minioSecretName, metav1.GetOptions{})
	if err != nil {
		log.Fatalln(err)
	}
	accessKey := string(secret.Data["accesskey"])
	secretKey := string(secret.Data["secretkey"])
	return accessKey, secretKey
}

type patchValue struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value string `json:"value"`
}

func LabelNodes() {
	c, err := GetConfig()
	if err != nil {
		log.Fatalln(err)
	}
	client, err := kubernetes.NewForConfig(c)
	if err != nil {
		log.Fatalln(err)
	}
	ip, _ := GetKubernetesIP()
	ip = SanitizeString(ip)
	patch := patchValue{
		Op:    "add",
		Path:  "/metadata/labels/irtsb.cluster",
		Value: ip,
	}
	b, err := json.Marshal([]patchValue{patch})
	if err != nil {
		log.Fatalln(err)
	}
	list, err := client.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Fatalln(err)
	}
	for _, node := range list.Items {
		_, err := client.CoreV1().Nodes().Patch(context.TODO(), node.Name, types.JSONPatchType, b, metav1.PatchOptions{})
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func SanitizeString(myString string) string {
	// Remove forbiden characters
	myString = strings.Replace(myString, "_", "-", -1)
	myString = strings.Replace(myString, "https://", "", -1)
	myString = strings.Replace(myString, ":", "-", -1)
	myString = strings.Replace(myString, ".", "-", -1)

	//	We truncate the string at 52 chars
	if len(myString) > 52 {
		myString = string(myString[:52])
	}
	return myString
}
