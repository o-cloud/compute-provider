package kubernetes_client

// To convert json directly to a go struct use the following link
// https://mholt.github.io/json-to-go/

type DataSecret struct {
	Kind           string      `json:"kind"`
	APIVersion     string      `json:"apiVersion"`
	Preferences    Preferences `json:"preferences"`
	Clusters       []Clusters  `json:"clusters"`
	Users          []Users     `json:"users"`
	Contexts       []Contexts  `json:"contexts"`
	CurrentContext string      `json:"current-context"`
}
type Preferences struct {
}
type Cluster struct {
	Server                   string `json:"server"`
	CertificateAuthorityData string `json:"certificate-authority-data"`
}
type Clusters struct {
	Name    string  `json:"name"`
	Cluster Cluster `json:"cluster"`
}
type User struct {
	Token string `json:"token"`
}
type Users struct {
	Name string `json:"name"`
	User User   `json:"user"`
}
type Context struct {
	Cluster string `json:"cluster"`
	User    string `json:"user"`
}
type Contexts struct {
	Name    string  `json:"name"`
	Context Context `json:"context"`
}
