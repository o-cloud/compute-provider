package compute_model

type PrepareLocalClusterRequest struct {
	TargetNamespace string `json:"targetNamespace"`
}

type ComputeRequestMacaroon struct {
	ClusterMacaroonDestination string           `form:"destination"     json:"destination"`
	Resource                   ResourceWorkFlow `form:"resources" json:"resources"`
}

type ComputeValidationMacaroon struct {
	Macaroon             string           `form:"macaroon"  json:"macaroon"`
	Resources            ResourceWorkFlow `form:"resources" json:"resources"`
	AddressClusterTarget string           `form:"sender"    json:"sender"`
}

type ResourceRequest struct {
	Action   string
	Resource string
	Duration string
}

type TokenRequest struct {
	Identity  string `form:"identity" json:"identity"`
	Namespace string `form:"namespace" json:"namespace"`

	// URL to use to request a new token for Admiralty
	TokenRequestURL string `form:"tokenRequestURL" json:"tokenRequestURL"`

	// API address of the target kubernetes cluster to be used
	RemoteKubernetesApiURL string `form:"remoteKubernetesApiURL" json:"remoteKubernetesApiURL"`
}
type ResponseToken struct {
	Token              string `form:"token" json:"token"`
	ApiUrl             string `form:"apiUrl" json:"apiUrl"`
	ServiceAccountName string `form:"accountName" json:"accountName"`
	CertificateData    string `form:"certificate" json:"certificate"`
	TargetName         string `form:"targetname" json:"targetname"`
	Namespace          string `form:"namespace" json:"namespace"`
}

type MacaroonMongoDB struct {
	Macaroon string
	//cluster creator macaroon
	AddressAdmiraltyTarget string
	//cluster use macaroon
	ClusterMacaroonDestination string
}

type ResourceWorkFlow struct {
	CPU              string `form:"cpu"  json:"cpu"`
	RAM              string `form:"ram"  json:"ram"`
	Duration         string `form:"duration"  json:"duration"`
	ExecutionStorage string `form:"storage"  json:"storage"`
}

type AvailableResources struct {
	CPU int `form:"cpu"  json:"cpu"`
	RAM int `form:"ram"  json:"ram"`
}
