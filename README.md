# Compute provider

## Basic structure of the compute providers 

The goal of the compute providers is:
* Give to the catalog the list of ressources of the cluster
* Take care of configuring Admiralty and Argo on the required clusters
* Implement macaroons


## Compute provider endpoints


| Request | Endpoint | Action   |
| :------------- | :----------  | :-----------  |
| POST | api/compute/macaroon   | Create a macaroon |
| POST | api/compute/macaroon/validation   | Validate a macaroon |
| GET  | api/compute/macaroon  | Get a macaroon |
| GET  | api/compute/token  | Request a token for Admiralty configuration |
| POST | api/compute/token  | Tell compute provider to retrieve token from distant compute provider |
| GET  | api/compute/resources/delete/serviceaccount  | Delete `ServiceAccount` |
| GET  | api/compute/resources/delete/namespace  | Delete `Namespace` |


### Basic description of the compute provider.
The graph represents the interaction between two compute providers (CP1,CP2)
and their kubernetes clusters for the installation of Admiralty.

```plantuml
@startuml
participant PPO1 as PPO1
participant cluster1 as cluster1
participant CP1 as CP1
participant CP2 as CP2
participant cluster2 as cluster2
database DB2 as DB2
CP2 -> CP1 : Send Valid Macaroon (after condition)
PPO1 -> CP1 : Send order to request token and connection parameters CP2
CP1 -> CP2 : Token and connection parameters request
CP2 -> cluster2: Service account creation
cluster2 -> CP2: Get Token from service account created
cluster2 -> CP2: Get cluster X.509 certificate
CP2 -> cluster2: Create Source cluster for cluster1
CP2 -> DB2 : (Store token, ServiceAccount and ValidUntil) Do we still do that?
CP2 -> CP1 : Send Token and connection parameters
CP1 -> cluster1 : Create secret for cluster 2
CP1 -> cluster1 : Create target for cluster2
cluster1 -> cluster2 : Admiralty workflow deployment
CP2 -> cluster2: Delete service account once workflow is done
@enduml
```

## Configuration of Admiralty between 2 clusters via the compute provider. 
In order to configure Admiralty between two clusters, Admiralty has to be installed on both clusters. 
This step is done with the [umbrella chart](https://gitlab.com/o-cloud/helm-charts). In case Admiralty has not been installed with the ubrella chart, you can use 
[this bash script](https://gitlab.com/o-cloud/admiralty_mcs/-/blob/master/scripts/set_multicluster-scheduler.sh) or follow 
the [Admiralty quick start guide](https://admiralty.io/docs/quick_start)

### Generate the token request
The first step is to generate the token request from the compute provider (CP) 1 to CP2.
This is done by the pipeline orchestrator of cluster 1. The pipeline orchestrator has to make a POST request 
on CP1 at the endpoint `http://XX.XX.XX.XX/api/compute/token` where `XX.XX.XX.XX`is the ip address the compute
provider is listening on.
The body of the POST request is as follow

```json
{
    "tokenRequestURL": "http://35.233.117.59/api/compute/token?identity=clusternew1&namespace=workflow-namespace",
    "remoteKubernetesApiURL": "https://35.241.237.173:443"
}
```
Where 
* `tokenRequestURL`is the URL at which the token will be requested, i.e., the CP2 address. 
	* `identity` parameter in the URL is the name of the cluster1 (used to set the name of the `ServiceAccount` created on cluster2).
	* `namespace` is the namespace used to run admiralty and argo jobs/workflows
* `remoteKubernetesApiRUL` is the URL of the cluster2 kubernetes API, needed by Admiralty

### Request the token
The CP1 receives the POST request from the pipeline orchestrator (cluster 1), extracts the `tokenRequestURL` and sends a GET request
to the CP2 at the URL given in `tokenRequestURL`. Note that, for now, the `identity` parameter has to be set by the pipeline orchestrator.
The `namespace` parameter has to follow the convention.
The CP2, on receiving the request, will create the `namespace` if does not already exists and a `ServiceAccount` on the cluster 2 together
with a`Target` ressource used by Admiralty.
It will, from that `ServiceAccount`, extract a token and store it in a `ResponseToken` structure, together with the name of the `ServiceAccount` created, the name of the `Target`
ressource and the X.509 cluster certificate. The `ResponseToken` is then returned to the CP1 as a response from the GET request. 
The CP1 will create the `namespace` if does not already exists and a secret containing the token, the X.509 certificate from cluster 2 and the `remoteKubernetesApiURL`. 
It will also create a `Source` ressource on cluster1. From that point, cluster 1 should have created a 
virtual node representing cluster 2 and Admiralty is set between both clusters.

### Delete serviceaccount and namespace
To delete a `ServiceAccount` make a GET request at `http://XX.XX.XX.XX/api/compute/resources/delete/namespace?namespace=$NAMESPACE`
Where is the IP address of the compute provider and `$NAMESPACE` the name of the namespace to delete

To delete a `ServiceAccount` make a GET request at `http://XX.XX.XX.XX/api/compute/resources/delete/serviceaccount?serviceaccount=$NAMESPACE&namespace=$NAMESPACE`
Where is the IP address of the compute provider, `$NAMESPACE` the name of the namespace to delete and `$ServiceAccount` the name of the `ServiceAccount` to delete.


## Resources Env vars

To declare resources that can be used on the cluster the following environement variables can be use on the initialisation:

```ini
MAX_CPU=4
MAX_MEM=8347951104  # in bytes
#TODO manage other resources
```

If no variables have be set, the provider will try to retrieve the resources avaible on the cluster.

## To Do
* Create interface with catalog
* Create interface with Pipeline orchestrator
