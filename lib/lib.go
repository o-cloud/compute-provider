package serverLib

type ClusterServiceAccount struct {
	Token          string
	IP             string
	ServiceAccount string
}

type ClientRequest struct {
	Request string
	Target  string
	Origin  string
}

func (c ClusterServiceAccount) String() string {
	s := c.Token + " " + c.IP + " " + c.ServiceAccount
	return s
}

func (c ClientRequest) String() string {
	s := c.Request + " " + c.Target
	return s
}
