package resources_provider

import (
	"log"
)

func Init() {
	var cpu, mem = GetResources(true)
	log.Printf("Resources available: CPU:%d, Memory:%d\n", cpu, mem)
	log.Println("TODO Register resources to catalogue")
}
