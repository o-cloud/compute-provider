package resources_provider

import (
	"compute/kubernetes_client"
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	//	"os"

	//
	// Uncomment to load all auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth"
	//
	// Or uncomment to load specific auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth/azure"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/openstack"
)

func readEnvs() (int, int) {
	var max_cpu_str, max_mem_str string
	var max_cpu, max_mem int
	var err error

	max_cpu_str = os.Getenv("MAX_CPU")
	max_mem_str = os.Getenv("MAX_MEM")

	max_cpu, err = strconv.Atoi(max_cpu_str)
	if err != nil {
		log.Println("Unable to read MAX_CPU: ", err)
	}

	max_mem, err = strconv.Atoi(max_mem_str)
	if err != nil {
		log.Println("Unable to read MAX_MEM: ", err)
	}
	return max_cpu, max_mem
}

func gatherClusterResources(config *rest.Config) (int, int) {
	var max_cpu, max_mem int64

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	// get pods in all the namespaces by omitting namespace
	// Or specify namespace to get pods in particular namespace
	nodes, err := clientset.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
	}

	log.Printf("There are %d nodes in the cluster\n", len(nodes.Items))

	for _, node := range nodes.Items {
		//for _, field := range node.GetObjectMeta().GetManagedFields() {
		//	fmt.Printf("%T: %s : %s\n\n", field, field.Manager, "field.FieldsV1")
		//}
		var nodeName = node.Name
		var nodeMem, _ = node.Status.Capacity.Memory().AsInt64()
		var nodeCpu, _ = node.Status.Capacity.Cpu().AsInt64()

		fmt.Printf("%s\n Mem:%d\n CPU:%d \n", nodeName, nodeMem, nodeCpu)

		max_cpu += nodeCpu
		max_mem += nodeMem
	}
	return int(max_cpu), int(max_mem)
}

func GetResources(allowClusterScan bool) (int, int) {
	var max_cpu, max_mem, cluster_cpu, cluster_mem int

	max_cpu, max_mem = readEnvs()

	if allowClusterScan && (max_cpu == 0 || max_mem == 0) {
		log.Println("No env vars found, trying to gather cluster resources...")
		config, err := kubernetes_client.GetConfig()

		if err != nil {
			log.Fatalln(err)
		}

		if config != nil {
			cluster_cpu, cluster_mem = gatherClusterResources(config)

			if max_cpu == 0 {
				max_cpu = cluster_cpu
			}

			if max_mem == 0 {
				max_mem = cluster_mem
			}
		}
	}
	return max_cpu, max_mem
}
