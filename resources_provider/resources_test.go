package resources_provider

import (
	"fmt"
	"os"
	"testing"

	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

func Test_read_envs(t *testing.T) {
	tests := []struct {
		name     string
		want     int
		want1    int
		env_vars [2]int
	}{
		{name: "No env vars", want: 0, want1: 0},
		{name: "With env vars", want: 2, want1: 2000000, env_vars: [2]int{2, 2000000}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if len(tt.env_vars) > 0 {
				os.Setenv("MAX_CPU", fmt.Sprint(tt.env_vars[0]))
				os.Setenv("MAX_MEM", fmt.Sprint(tt.env_vars[1]))
			}
			got, got1 := readEnvs()
			if got != tt.want {
				t.Errorf("read_envs() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("read_envs() got1 = %v, want %v", got1, tt.want1)
			}
			os.Unsetenv("MAX_CPU")
			os.Unsetenv("MAX_MEM")
		})
	}
}

func Test_getResources(t *testing.T) {
	tests := []struct {
		name  string
		want  int
		want1 int
	}{
		{"No config", 0, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := GetResources(false)
			if got != tt.want {
				t.Errorf("getResources() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("getResources() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
