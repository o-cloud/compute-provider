module compute

go 1.15

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/validator/v10 v10.6.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/spf13/viper v1.8.1
	github.com/ugorji/go v1.2.6 // indirect
	gitlab.com/o-cloud/catalog v0.0.0-20211022075506-04fe74e90d3d
	gitlab.com/o-cloud/macaroon-security v0.0.0-20211025085131-d1ba3d205b2c
	go.mongodb.org/mongo-driver v1.5.2
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/sys v0.0.0-20210608053332-aa57babbf139 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/macaroon-bakery.v3 v3.0.0
	gopkg.in/macaroon.v2 v2.1.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	helm.sh/helm/v3 v3.6.1
	k8s.io/api v0.21.0
	k8s.io/apimachinery v0.21.1
	k8s.io/cli-runtime v0.21.0
	k8s.io/client-go v0.21.0
	rsc.io/letsencrypt v0.0.3 // indirect
)
