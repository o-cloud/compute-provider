package main

import (
	"compute/compute_provider"
	"compute/config"
	"compute/kubernetes_client"
	"fmt"
	"github.com/spf13/viper"
	"strings"

	"github.com/gin-gonic/gin"
	macaroonSecurity "gitlab.com/o-cloud/macaroon-security"
)

func main() {

	// Load configuration
	config.Load()

	//initialise mongoDB
	var uripattern string
	if strings.HasSuffix(viper.GetString("MONGO_CON_STRING"), "/") {
		uripattern = "%s%s"
	} else {
		uripattern = "%s/%s"
	}

	fulMongoURI := fmt.Sprintf(uripattern, viper.GetString("MONGO_CON_STRING"), "admin")
	macaroonApp := macaroonSecurity.Initialize(fulMongoURI)

	collection := compute_provider.InitMongoDB(viper.GetString("MONGO_CON_STRING"))

	computeHandler := compute_provider.ComputeHandler{
		MacaroonLib: macaroonApp,
		MongoDB:     collection,
	}
	kubernetes_client.LabelNodes()
	//initialise compute avec le catalog
	computeHandler.InitCatalog()

	r := gin.Default()
	v1 := r.Group("/api")
	computeHandler.RequestComputeAPI(v1.Group("/compute"))

	if err := r.Run(); err != nil {
		panic(err)
	}
}
