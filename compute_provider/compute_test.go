package compute_provider

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	macaroonSecurity "gitlab.com/o-cloud/macaroon-security"
	"gopkg.in/macaroon-bakery.v3/bakery"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestInitialise(t *testing.T) {

}

type testSetup struct {
	t      *testing.T
	engine *gin.Engine
	mc     *MockMacaroon
	req    *http.Request
	rr     *httptest.ResponseRecorder
}

type MockMacaroon struct {
	args        string
	called      string
	outRetArray []byte
	inRetArray  []byte
	err         error
}

// to continue
func getSetup(t *testing.T, method, url string, body interface{}) testSetup {

	req, err := http.NewRequest(method, url, nil)
	if body != nil {
		//byts, _ := json.Marshal(body)
		byte := body.([]byte)
		req, err = http.NewRequest(method, url, bytes.NewBuffer(byte))
	}
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	r := gin.New()
	mc := &MockMacaroon{}
	cph := ComputeHandler{
		MacaroonLib: mc,
	}
	v1 := r.Group("/api")
	cph.RequestComputeAPI(v1.Group("/compute"))

	//Pré setup de données

	return testSetup{
		t:      t,
		engine: r,
		mc:     mc,
		req:    req,
		rr:     rr,
	}
}

func TestCreateMacaroon(t *testing.T) {
	test := getSetup(t, "POST", "/api/compute/macaroon", []byte(`{"destination": "cluster1","resources": {"cpu": "write", "ram": "file", "duration": "10m","storage": "1GB" }}`))
	test.run()
	test.expectMethodCall("CreateRequest({cluster1 [{write CPU 10m} {file RAM 10m} {1GB STORAGE 10m}]})")
	test.expectResponseStatus(http.StatusCreated)
}

func TestValidationMacaroon(t *testing.T) {

	body := "{\n\"macaroon\": \"eyJjIjpbeyJpIjoiY2x1c3RlcjIsd3JpdGUsZmlsZSxkYXRlIGVuZCwyMDIxLTA2LTE3VDEyOjIwOjM2KzAwMDAifSx7ImkiOiJjbHVzdGVyMixyZWFkLGZpbGUyLGRhdGUgZW5kLDIwMjEtMDYtMTdUMTI6MTU6MzYrMDAwMCJ9XSwibCI6ImxvY2FsIiwiaTY0IjoiQXdvUUFEejZkYUVmb2x5dDlmRDlZbjdpVWhJZ05ESTFaR0V6WVRjMk1HUTRZakZqTnpNNVl6ZzBOalUxTldJeU9UZGxZamdhRUFvSVkyeDFjM1JsY2pJU0JISmxZV1EiLCJzNjQiOiJDSTdwY0I1N0hZcnFWMTByUUJsMFZKRU13LXMyYThZMmxLNkliVWNfcUZVIn0=\",\n    \"sender\": \"cluster2\",\n \"resources\": [\n{\"cpu\": \"3\",\n\"ram\": \"1GB\",\n \"duration\": \"25m\",\n \"storage\": \"1GB\"},\n\n{\"cpu\": \"3\",\n\"ram\": \"1GB\",\n \"duration\": \"25m\",\n \"storage\": \"1GB\"\n}\n]\n}"
	test := getSetup(t, "POST", "/api/compute/macaroon/validation", []byte(body))
	test.run()
	test.expectMethodCall("ReadResource(eyJjIjpbeyJpIjoiY2x1c3RlcjIsd3JpdGUsZmlsZSxkYXRlIGVuZCwyMDIxLTA2LTE3VDEyOjIwOjM2KzAwMDAifSx7ImkiOiJjbHVzdGVyMixyZWFkLGZpbGUyLGRhdGUgZW5kLDIwMjEtMDYtMTdUMTI6MTU6MzYrMDAwMCJ9XSwibCI6ImxvY2FsIiwiaTY0IjoiQXdvUUFEejZkYUVmb2x5dDlmRDlZbjdpVWhJZ05ESTFaR0V6WVRjMk1HUTRZakZqTnpNNVl6ZzBOalUxTldJeU9UZGxZamdhRUFvSVkyeDFjM1JsY2pJU0JISmxZV1EiLCJzNjQiOiJDSTdwY0I1N0hZcnFWMTByUUJsMFZKRU13LXMyYThZMmxLNkliVWNfcUZVIn0=)")
	test.expectResponseStatus(http.StatusOK)
}

func TestGetMacaroon(t *testing.T) {
	test := getSetup(t, "GET", "/api/compute/macaroon", nil)
	test.run()
	test.expectMethodCall("")
	test.expectResponseStatus(http.StatusOK)

}

//Helper func
func (ts *testSetup) run() {
	ts.engine.ServeHTTP(ts.rr, ts.req)
}

// Check the response code is what we expect.
func (ts *testSetup) expectResponseStatus(expectedCode int) {
	if ts.rr.Code != expectedCode {
		ts.t.Errorf("handler returned wrong status code: got %v want %v",
			ts.rr.Code, expectedCode)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectBodyContent(expectedBody string) {
	if ts.rr.Body.String() != expectedBody {
		ts.t.Errorf("handler returned unexpected body: got %v want %v",
			ts.rr.Body.String(), expectedBody)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMethodCall(expectedMethod string) {
	if ts.mc.called != expectedMethod {
		ts.t.Errorf("expected method not called: got %v want %v",
			ts.mc.called, expectedMethod)
	}
}

func (mc *MockMacaroon) CreateRequest(request macaroonSecurity.MacaroonRequest) (*bakery.Macaroon, error) {
	mc.called = fmt.Sprintf("%sCreateRequest(%s)", mc.called, request)
	var id = []byte("test")
	var rootKey = []byte("testKey")
	varMacaroon, _ := bakery.NewMacaroon(rootKey, id, "", bakery.Version2, nil)
	return varMacaroon, nil
}

func (mc *MockMacaroon) ReadResource(jsonData []byte) ([]bakery.Op, []string, error) {
	mc.called = mc.called + fmt.Sprintf("%sReadResource(%s)", mc.called, jsonData)
	return nil, nil, nil
}

func (mc *MockMacaroon) ModifyMaroon(jsonData []byte) ([]byte, error) {
	//not use
	panic("implement me")
}
