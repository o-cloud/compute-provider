/* JSON EchoClient
 */
package compute_provider

import (
	"compute/compute_model"
	"compute/helm_client"
	"compute/kubernetes_client"
	"compute/resources_provider"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/spf13/viper"

	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/catalog/api/intern"
	"gitlab.com/o-cloud/catalog/api/public"
	macaroonSecurity "gitlab.com/o-cloud/macaroon-security"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/macaroon.v2"

	//"reflect"
	"strings"
	"time"
)

func (handler *ComputeHandler) RequestComputeAPI(request *gin.RouterGroup) {
	//creation
	request.POST("/macaroon", handler.CreateMacaroon)
	//validation
	request.POST("/macaroon/validation", handler.ValidationMacaroon)
	//get macaroon create for the sender in the past
	request.GET("/macaroon", handler.GetMacaroonBySender)
	// request token
	//request.GET("/token", RequestToken)
	request.GET("/token", handler.SendTokenAfterRequestFromComputeProvider)
	// Pipeline orchestrator is asking to retrieve a token
	request.POST("/token", handler.RequestTokenToComputeProvider)
	// Called by the pipeline orchestrator before executing a workflow.
	request.POST("/prepareLocalCluster", handler.PrepareLocalClusterForWfExecution)
	// delete service account
	request.GET("/resources/delete/serviceaccount", handler.DeleteServiceAccount)
	// delete service account
	request.GET("/resources/delete/namespace", handler.DeleteNamespace)
}

func (handler *ComputeHandler) DeleteNamespace(context *gin.Context) {
	namespace := context.Query("namespace")

	config, err := kubernetes_client.GetConfig()
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("fail retrieve kubernetes config"))
		context.Error(err)
		return
	}

	kubernetes_client.DeleteNamespace(namespace, config)
}

func (handler *ComputeHandler) DeleteServiceAccount(context *gin.Context) {
	namespace := context.Query("namespace")
	serviceAccount := context.Query("serviceaccount")

	config, err := kubernetes_client.GetConfig()
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("fail to retrieve kubernetes config"))
		context.Error(err)
		return
	}

	kubernetes_client.DeleteServiceAccount(serviceAccount, namespace, config)
}

func (handler *ComputeHandler) GetMacaroonBySender(context *gin.Context) {
	var tokenRequest compute_model.TokenRequest
	tokenRequest.Identity = context.Query("identity")
	fmt.Println("Identity: ", tokenRequest.Identity)
	result, err := find(handler.MongoDB, tokenRequest.Identity)
	if err != nil {
		context.JSON(http.StatusNotFound, fmt.Errorf("the macaroon is not valide"))
		return
	}
	context.JSON(http.StatusOK, result.Macaroon)

}

func (handler *ComputeHandler) PrepareLocalClusterForWfExecution(c *gin.Context) {
	var request compute_model.PrepareLocalClusterRequest
	if err := c.BindJSON(&request); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusUnprocessableEntity)
		return
	}
	config, err := kubernetes_client.GetConfig()
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	//Try to create the required namespace and service account
	err = kubernetes_client.CreateNameSpace(request.TargetNamespace, config)
	if err != nil {
		log.Println(err)
	}
	err = kubernetes_client.CreateRbacForArgo(request.TargetNamespace, config)
	if err != nil {
		log.Println(err)
	}
	c.Status(http.StatusOK)
}

func (handler *ComputeHandler) RequestTokenToComputeProvider(context *gin.Context) {

	jsonData, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("wrong body format"))
		return
	}

	// Retrieve info from the POST request
	var tokenRequest compute_model.TokenRequest
	json.Unmarshal(jsonData, &tokenRequest)

	log.Println("Requesting token")
	//log.Println("Remote compute provider is at: ", tokenRequest.TokenRequestURL)
	tokenRequest.Namespace = kubernetes_client.GetNamespaceFromUrl(tokenRequest.TokenRequestURL)
	log.Println("Namespace to use for Admiralty is: ", tokenRequest.Namespace)

	// Sending token request
	response, err := http.Get(tokenRequest.TokenRequestURL)
	if err != nil {
		fmt.Println(err)
	}
	log.Println("Token request answered with status code ", response.StatusCode)

	// Get reponse from token request
	var responseBody map[string]interface{}
	json.NewDecoder(response.Body).Decode(&responseBody)

	var responseToken compute_model.ResponseToken

	responseToken.Token = responseBody["token"].(string)
	responseToken.ApiUrl = responseBody["apiUrl"].(string)
	log.Println("Target Kubernetes API is at: ", responseToken.ApiUrl)
	responseToken.ServiceAccountName = responseBody["accountName"].(string)
	responseToken.CertificateData = responseBody["certificate"].(string)
	responseToken.TargetName = kubernetes_client.SanitizeString(responseToken.ApiUrl)
	responseToken.Namespace = responseBody["namespace"].(string)

	config, err := kubernetes_client.GetConfig()
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("error retrieving kubernetes config"))
		return
	}

	// This method creates the secret for the Source cluster
	// from the config of the target serviceaccount
	secret, err := kubernetes_client.BuildSecretFromServiceAccountInfo(responseToken)
	if err != nil {
		fmt.Println(err)
	}

	kubernetes_client.ApplySecret(secret, tokenRequest.Namespace, config)
	helm_client.InstallTargetChartOnSourceCluster(responseToken.TargetName, secret.Name, tokenRequest.Namespace)
	kubernetes_client.AdmiraltyLabelNamespace(tokenRequest.Namespace, config)

	// This will be set once the Umbrella chart is ready
	//kubernetes_client.SetRuntimeContainer(config)

	context.JSON(http.StatusOK, nil)

}

func (handler *ComputeHandler) SendTokenAfterRequestFromComputeProvider(context *gin.Context) {
	// This method is called when the compute provider receives a token request from
	// another compute provider. It will create the SA and the Target CRD, and return
	// a ResponseToken.

	var tokenRequest compute_model.TokenRequest

	// Object that is to be returned. Holds the info about service account
	var response compute_model.ResponseToken

	tokenRequest.Identity = context.Query("identity")
	tokenRequest.Namespace = context.Query("namespace")

	config, err := kubernetes_client.GetConfig()
	if err != nil {
		fmt.Println(err)
	}

	if tokenRequest.Namespace != "default" {
		err := kubernetes_client.CreateNameSpace(tokenRequest.Namespace, config)
		if err != nil {
			log.Println(err)
		}
		err = kubernetes_client.CreateRbacForArgo(tokenRequest.Namespace, config)
		if err != nil {
			log.Println(err)
		}
	}

	serviceAccountName := kubernetes_client.GenerateServiceAccountName(tokenRequest.Identity)
	fmt.Println(tokenRequest.Identity)

	// Create response to send back to the compute provider
	response, err = kubernetes_client.DeployServiceAccount(serviceAccountName, tokenRequest.Namespace, config)
	if err != nil {
		fmt.Println(err)
	}
	response.Namespace = tokenRequest.Namespace

	response.ApiUrl = kubernetes_client.GetKubernetesEndpoint()
	log.Println("APIURL ON TARGET:", response.ApiUrl)

	// Create new Target for that compute provider
	helm_client.InstallSourceChartOnTargetCluster(serviceAccountName, tokenRequest.Namespace)
	kubernetes_client.AdmiraltyLabelNamespace(tokenRequest.Namespace, config)

	// This will be set once the Umbrella chart is ready
	//kubernetes_client.SetRuntimeContainer(config)

	// Return the response Token that has to be mounted in the secret on the Source Target
	context.JSON(http.StatusOK, response)
}

func (handler *ComputeHandler) ValidationMacaroon(context *gin.Context) {
	jsonData, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("the body has not good format"))
		return
	}
	var validationMacaroon compute_model.ComputeValidationMacaroon
	json.Unmarshal(jsonData, &validationMacaroon)
	operation, condition, err := handler.MacaroonLib.ReadResource([]byte(validationMacaroon.Macaroon))
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, "the macaroon is not valide")
		return
	}
	println(operation)
	println(condition)

	decode, err := base64.StdEncoding.DecodeString(validationMacaroon.Macaroon)
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, "the macaroon is not valide")
		return
	}

	var mac macaroon.Macaroon
	if err := mac.UnmarshalJSON(decode); err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("the macaroon is not valide"))
		return
	}

	resources := buildResources(validationMacaroon.Resources)
	// validation rights for each resource
	for _, cav := range mac.Caveats() {
		cavId := string(cav.Id)
		for _, resource := range resources {
			if strings.Contains(cavId, resource.Resource) && strings.Contains(cavId, resource.Action) && strings.Contains(cavId, validationMacaroon.AddressClusterTarget) {
				result := macaroonSecurity.ValidationCaveatDate(cavId)
				if result == false {
					context.JSON(http.StatusBadRequest, "the right on resource is expired")
					return
				}
				//add comparison between duration in body and caveat
				if resource.Duration != "" {
					resultCompare := compareDuration(resource.Duration, cavId)
					if resultCompare == true {
						context.JSON(http.StatusBadRequest, "the duration in resource request is greater then your token")
						return
					}
				}
			}
		}
	}
}

func compareDuration(duration string, id string) bool {
	layoutISO := "2006-01-02T15:04:05-0700"
	currenTimeVar := time.Now().UTC()
	variables := strings.Split(string(id), ",")
	date := variables[len(variables)-1]
	dateTime, err := time.Parse(layoutISO, date)
	if err != nil {
		return false
	}
	durationTime, err := time.ParseDuration(duration)
	if err != nil {
		return false
	}
	resourceTime := currenTimeVar.Add(durationTime)
	// if the duration of resource request is greater than macaroon right the system gives bad request
	return resourceTime.After(dateTime)

}

// CreateMacaroon modify librarie pour prendre en compte plus des caveats
func (handler *ComputeHandler) CreateMacaroon(context *gin.Context) {

	jsonData, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("the body has not good format"))
		return
	}
	var requestValidator compute_model.ComputeRequestMacaroon
	json.Unmarshal(jsonData, &requestValidator)

	deleteOldMacaroon(handler.MongoDB, requestValidator.ClusterMacaroonDestination)

	var resources []compute_model.ResourceRequest
	resources = buildResources(requestValidator.Resource)

	var resourcesToCreate []macaroonSecurity.ResourceRequest
	for _, resource := range resources {
		var resourceRequest = macaroonSecurity.ResourceRequest{
			Action:   resource.Action,
			File:     resource.Resource,
			Duration: resource.Duration,
		}
		resourcesToCreate = append(resourcesToCreate, resourceRequest)
	}

	requestCompute := macaroonSecurity.MacaroonRequest{
		Sender:     requestValidator.ClusterMacaroonDestination,
		Ressources: resourcesToCreate,
	}
	result, err := handler.MacaroonLib.CreateRequest(requestCompute)
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("fail in creation macaroon"))
		return
	}
	response, err := result.MarshalJSON()
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, fmt.Errorf("fail in conversion JSON of macaroon"))
		return
	}

	storeMacaroon := compute_model.MacaroonMongoDB{
		Macaroon:                   string(response),
		AddressAdmiraltyTarget:     "cluster2",
		ClusterMacaroonDestination: requestValidator.ClusterMacaroonDestination,
	}
	resultInsert := insert(handler.MongoDB, storeMacaroon)
	if resultInsert == false {
		println("no insertion in mongoDB")
	}
	context.JSON(http.StatusCreated, response)
}

func buildResources(resourcesIn compute_model.ResourceWorkFlow) []compute_model.ResourceRequest {
	var resources []compute_model.ResourceRequest
	resource := compute_model.ResourceRequest{
		Action:   resourcesIn.CPU,
		Duration: resourcesIn.Duration,
		Resource: "CPU",
	}
	resources = append(resources, resource)

	resource = compute_model.ResourceRequest{
		Action:   resourcesIn.RAM,
		Duration: resourcesIn.Duration,
		Resource: "RAM",
	}
	resources = append(resources, resource)

	resource = compute_model.ResourceRequest{
		Action:   resourcesIn.ExecutionStorage,
		Duration: resourcesIn.Duration,
		Resource: "STORAGE",
	}

	resources = append(resources, resource)
	return resources
}

func deleteOldMacaroon(collection *mongo.Collection, sender string) {
	delete(collection, sender)
}

func (handler *ComputeHandler) InitCatalog() {
	var providerInfo = setterInfoComputeProvider()

	var catalogURL = fmt.Sprintf("%s/api/catalog/internal", viper.GetString("CATALOG_API"))
	internCatalog := intern.NewClient(catalogURL)
	if err := internCatalog.RegisterDataProvider(context.Background(), providerInfo); err != nil {
		log.Println("Unable to register provider to catalog")
		log.Printf("Check the Catalog Api endpoint (%s)\n", viper.GetString("CATALOG_API"))
		panic(err)
	}

	var catalogURLPublic = fmt.Sprintf("%s/api/catalog/public", viper.GetString("CATALOG_API"))
	publicCatalog := public.NewClient(catalogURLPublic)
	providerInfoPublic, _ := publicCatalog.GetProviderInfo(context.Background(), public.Computing, "computerprovider1")
	print(providerInfoPublic.ProviderId)
}

func setterInfoComputeProvider() intern.ProviderInternalInfo {
	var max_cpu, max_mem = resources_provider.GetResources(true)
	var accessKey, secretKey = kubernetes_client.GetS3Keys()
	ip, _ := kubernetes_client.GetKubernetesIP()
	metadata := map[string]interface{}{
		"resources": compute_model.AvailableResources{
			CPU: max_cpu,
			RAM: max_mem,
		},
		"kubernetesApi": kubernetes_client.GetKubernetesEndpoint(),
		"s3": map[string]interface{}{
			"accessKey": accessKey,
			"secretKey": secretKey,
		},
	}
	return intern.ProviderInternalInfo{
		Id:                "",
		ProviderId:        "computerprovider1-" + ip,
		Type:              public.Computing,
		Version:           "0",
		Name:              "computerProvider",
		Description:       "enregistrement computer provider ",
		EndpointLocation:  "{{CLUSTER_HOST}}/api/compute/",
		WorkflowStructure: "",
		Metadata:          metadata,
	}
}

type ComputeHandler struct {
	MacaroonLib macaroonSecurity.IMacaroonSecurity
	MongoDB     *mongo.Collection
}
