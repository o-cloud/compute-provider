package compute_provider

import (
	"compute/compute_model"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
	"log"
)

const (
	AuthDatabase = "local"
	Collection   = "macaroon"
)

func InitMongoDB(mongoDBHost string) *mongo.Collection {
	//mongoDBHost = "mongodb://mongodb:27017"
	clientOptions := options.Client().ApplyURI(mongoDBHost)
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatalln(err)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	collection := client.Database(AuthDatabase).Collection(Collection)
	return collection
}

func insert(collection *mongo.Collection, storeMacaroon compute_model.MacaroonMongoDB) bool {
	//for testing
	if collection == nil {
		return true
	}
	result, error := collection.InsertOne(context.TODO(), storeMacaroon)
	if error != nil {
		println("no document insert with sender selected")
		return false
	}
	print(result)
	return true
}

func delete(collection *mongo.Collection, sender string) error {
	//for testing
	if collection == nil {
		return nil
	}
	resultDelete, err := collection.DeleteOne(context.TODO(), bson.M{"clustermacaroondestination": sender})

	if err != nil {
		return err
	}
	if resultDelete.DeletedCount == 0 {
		println("no document found with sender selected")
	}
	return nil
}

func find(collection *mongo.Collection, sender string) (compute_model.MacaroonMongoDB, error) {
	if collection == nil {
		return compute_model.MacaroonMongoDB{}, nil
	}
	result := compute_model.MacaroonMongoDB{}
	err := collection.FindOne(context.TODO(), bson.M{"clustermacaroondestination": sender}).Decode(&result)
	if err != nil {
		return compute_model.MacaroonMongoDB{}, err
	}
	return result, nil
}
